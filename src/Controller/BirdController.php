<?php

namespace App\Controller;

use App\Entity\Birds;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ProductRepository;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

use App\Document\Logger;
use Doctrine\ODM\MongoDB\DocumentManager;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Nelmio\ApiDocBundle\NelmioApiDocBundle;


class BirdController extends AbstractController
{

    /**
     * @Route("/bird", name="bird_index", methods={"GET"})
     */
            /**
         * Добавление птицы
         * @SWG\Tag (name="Птица")
         * @SWG\Response (
         *     response=200,
         *     description="Добавление птицы"
         * )
         * @param Request $request
         * @return View
         */

    public function index(DocumentManager $dm): Response
    {
        $cachePool = new FilesystemAdapter('', 0, "cache");
        //return $this->render('bird/index.html.twig', [
          //  'controller_name' => 'BirdController',
        //]);
                // you can fetch the EntityManager via $this->getDoctrine()
        // or you can add an argument to the action: createProduct(EntityManagerInterface $entityManager)
        $entityManager = $this->getDoctrine()->getManager();

        $product = new Birds();
        $product->setName('Kesha');
        $product->setPrice(1999);
        $product->setDescription('Kesha good!');
        $product->id = 1;

        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $entityManager->persist($product);

        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();
        //REDIS
        $demoString = $cachePool->getItem(strval($product->getId()));
        $sp = array();
        $sp[0] = '1';
        $sp[1] = 'KeshaRadis';
        $sp[2] = 19991;
        $sp[3] = 'Kesha good!Radis';
        $this->AddToChannel(serialize($sp));
        //$sp= serialize($product);
        if (!$demoString->isHit())
        {
            $demoString->set($sp);
            $cachePool->save($demoString);
        }
        
        
        $productD = new Logger();
        $productD->setids($product->getId());
        $productD->setoperation('Add');

        $dm->persist($productD);
        $dm->flush();
        
//$symfony_version = \Symfony\Component\HttpKernel\Kernel::VERSION; 
        //return new Response('Saved new bird with id '.$product->getId().$symfony_version);
        echo $productD->getId();
        echo "идс".$productD->getids();
        return new Response('Saved new bird with id '.$product->getId());

        
    }
    
        /**
         * @Route("/bird/{id}", name="bird_show")
         */
         
         
        public function show($id,DocumentManager $dm)
        {
            $cachePool = new FilesystemAdapter('', 0, "cache");

                if ($cachePool->hasItem(strval($id)))
                {
                    $demoString = $cachePool->getItem(strval($id));
                    $bird = $demoString->get();
                    echo "heh";
                }
                else
                {
                   $bird = $this->getDoctrine()
                        ->getRepository(Birds::class)
                        ->find($id);

                    if (!$bird) {
                        throw $this->createNotFoundException(
                            'No bird found for id '.$id
                        );
                    }
                }
                var_dump($bird);
                $productD = new Logger();
                $productD->setids($id);
                $productD->setoperation('Get');

                $dm->persist($productD);
                $dm->flush();
                
        //$symfony_version = \Symfony\Component\HttpKernel\Kernel::VERSION; 
                //return new Response('Saved new bird with id '.$product->getId().$symfony_version);
                echo $productD->getId();
                echo "идс".$productD->getids();
                
            return new Response('Check out this great bird: '.$bird[1]);

            // or render a template
            // in the template, print things with {{ product.name }}
            // return $this->render('product/show.html.twig', ['product' => $product]);
        }
        
                /**
         * @Route("/bird/edit/{id}")
         */
        public function update($id, DocumentManager $dm)
        {
            $cachePool = new FilesystemAdapter('', 0, "cache");
            $entityManager = $this->getDoctrine()->getManager();
            $bird = $entityManager->getRepository(Birds::class)->find($id);

            if (!$bird) {
                throw $this->createNotFoundException(
                    'No bird found for id '.$id
                );
            }

            $bird->setName('New bird name!');
            $entityManager->flush();
            
                $demoString = $cachePool->getItem(strval($id));
                if ($demoString->isHit())
                {
                    $demoString->set(serialize($bird));
                    $cachePool->save($demoString);
                    $bird2 = unserialize($demoString->get());
                    echo $bird2->getName();
                }
            
            
                $productD = new Logger();
                $productD->setids($bird->getId());
                $productD->setoperation('update');

                $dm->persist($productD);
                $dm->flush();
                echo $productD->getId();
            echo "идс".$productD->getids();
            return $this->redirectToRoute('bird_show', [
                'id' => $bird->getId()
            ]);
        }
        
                        /**
         * @Route("/bird/delete/{id}")
         */
        public function delete($id,  DocumentManager $dm)
        {
            $cachePool = new FilesystemAdapter('', 0, "cache");
            $cachePool->deleteItem(strval($id));
             
            if (!$cachePool->hasItem(strval($id)))
            {
                echo "The cache entry demo_array was deleted successfully!\n";
            }


            $entityManager = $this->getDoctrine()->getManager();
            $bird = $entityManager->getRepository(Birds::class)->find($id);

            if (!$bird) {
                throw $this->createNotFoundException(
                    'No bird found for id '.$id
                );
            }
            $this->AddToChannelDel(serialize($id));
            $entityManager->remove($bird);
            $entityManager->flush();
            
                            $productD = new Logger();
                $productD->setids($id);
                $productD->setoperation('delete');

                $dm->persist($productD);
                $dm->flush();
                echo $productD->getId();
                
            return $this->redirectToRoute('bird_index'
            );
        }
        
        private function AddToChannel($sp)
        {
            $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
            //Берем канал и декларируем в нем новую очередь, первый аргумент - название
            $channel = $connection->channel();
            $channel->queue_declare('hello', false, false, false, false);

            //Создаем новое сообщение
            $msg = new AMQPMessage($sp);
            //Отправляем его в очередь
            $channel->basic_publish($msg, '', 'hello');

            echo " [x] ".$sp."\n";

            //Не забываем закрыть канал и соединение
            $channel->close();
            $connection->close();

        }
        
        private function AddToChannelDel($sp)
        {
            $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
            //Берем канал и декларируем в нем новую очередь, первый аргумент - название
            $channel = $connection->channel();
            $channel->queue_declare('delete', false, false, false, false);

            //Создаем новое сообщение
            $msg = new AMQPMessage($sp);
            //Отправляем его в очередь
            $channel->basic_publish($msg, '', 'delete');

            echo " [x] ".$sp."\n";

            //Не забываем закрыть канал и соединение
            $channel->close();
            $connection->close();

        }
        
        private function logger($dm,$id)
        {
            $productD = new Logger();
                $productD->setids($id);
                $productD->setoperation('delete');

                $dm->persist($productD);
                $dm->flush();
                echo $productD->getId();
        }
}
