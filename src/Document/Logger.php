<?php
    // src/Document/Logger.php
namespace App\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
 
class Logger
{
     /**
     * @MongoDB\Id
     */
    protected $id;
    
    /**
     * @MongoDB\Field(type="string")
     */
    protected $ids;
    
       /**
     * @MongoDB\Field(type="string")
     */
    protected $operation;
    public function getId()
    {
        return $this->id;
    }
        public function getids()
    {
        return $this->ids;
    }
    
/**
 * Set ids
 *
 * @param string $ids
 * @return self
 */
    public function setids(string $ids): self
    {
        $this->ids = $ids;

        return $this;
    }
    
/**
 * Set operation
 *
 * @param string $operation
 * @return self
 */
    public function setoperation(string $operation): self
    {
        $this->operation = $operation;

        return $this;
    }
}
?>